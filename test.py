#Exercise 5: (Advanced) Change the socket program so that it only shows
#data after the headers and a blank line have been received. Remember
#that recv receives characters (newlines and all), not lines.
import sys
import socket
#try:
url = input('Enter an URL ')
if(url==""):
	url="http://data.pr4e.org/romeo.txt"
hostname=url.split('/')
if(hostname[0]!="http:" and hostname[0]!="https:"):
	print("You have input an improperly formatted or non-existent URL. ")
	exit()
if((hostname[0]=="http:" or hostname[0]=="https:") and hostname[1]==''):
	hostname=hostname[2]
	

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect((hostname, 80))
cmd = 'GET '+str(url)+' HTTP/1.0\r\n\r\n'
cmd = cmd.encode()
mysock.send(cmd)

firsthit=True
while True:
	data = mysock.recv(512)
	if len(data) < 1:
		break
	datastr=str(data.decode())
	if(firsthit==True):
		num=datastr.find("text/plain")
		datastr=datastr[num+14:]
		firsthit=False
		print(datastr)
	else:
		print(data.decode(),end='')

mysock.close()
#except:
    #print("Unexpected error:", sys.exc_info()[0])