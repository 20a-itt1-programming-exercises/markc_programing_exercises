# Requirements for the number guessing game are:
# On game start calculate a random number between 0 and 9
# The computer will give you 3 attempts to guess the number, otherwise the computer wins the round
# If the user enters anything else than a number print the message Thats not even a number.... restarting and then restart the program
# If the user wins print Damn, you won!, try again? (y/n)
# If the computer wins, print na na I won :-P - try again? (y/n)
# If user enters y at the end of a round, restart the program
# If the user enters n at the end of a round, print the total rounds score between human and computer
# Test the program with following test cases:
import random
import re
def guess(attempt):
	guessstr=input("enter your guess between 0 and 9: ")
	try:
		guessint=int(guessstr)
#		first_digit = re.search('\d', guessstr)
#		if first_digit is not None:
#			guessint=int(guessstr[first_digit.start()])
#		else: 
#			print("Thats not even a number.... restarting")
#			return guess(attempt)
	except ValueError:
		print("Thats not even a number.... restarting")
		return guess(attempt)
	if(guessint==randomnumber):
		return True
	else:
		return False
		
wins=0
losses=0
randomnumber="";
done=False
while(done==False):
	randomnumber=random.randint(0, 9)
	won=guess(1)
	if(won==False):
		print("Wrong guess, try again!"+str(randomnumber))
		won=guess(2)
	if(won==False):
		print("Wrong guess, try again!")
		won=guess(3)
	if(won==False):
		losses+=1
		donext=input("na na I won :-P - try again? (y/n): ")
		if(donext=="y"):
			continue
		elif(donext=="n"):
			done=True
			break
	else:
		wins+=1
		donext=input("Damn, you won!, try again? (y/n): ")
		if(donext=="y"):
			continue
		elif(donext=="n"):
			done=True
			break		
	if(wins==5 or losses==5)
		done=True
		break
if(done==True):
	print('computer wins: '+str(losses)+' human wins: '+str(wins)+' see ya...')	
