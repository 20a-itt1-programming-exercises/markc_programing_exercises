#Exercise 1: Change the socket program socket1.py to prompt the user
#for the URL so it can read any web page. You can use split('/') to
#break the URL into its component parts so you can extract the host
#name for the socket connect call. Add error checking using try and
#except to handle the condition where the user enters an improperly
#formatted or non-existent URL.
import sys
import socket
try:
	url = input('Enter an URL ')
	if(url==""):
		url="http://data.pr4e.org/romeo.txt"
	hostname=url.split('/')
	if(hostname[0]!="http:" and hostname[0]!="https:"):
		print("You have input an improperly formatted or non-existent URL. ")
		exit()
	if((hostname[0]=="http:" or hostname[0]=="https:") and hostname[1]==''):
		hostname=hostname[2]
		
	print("Hostname ",hostname)

	mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	mysock.connect((hostname, 80))
	cmd = 'GET '+str(url)+' HTTP/1.0\r\n\r\n'
	cmd = cmd.encode()
	mysock.send(cmd)

	while True:
		data = mysock.recv(512)
		if len(data) < 1:
			break
		print(data.decode(),end='')

	mysock.close()
except:
    print("Unexpected error:", sys.exc_info()[0])


#Exercise 2: Change your socket program so that it counts the number
#of characters it has received and stops displaying any text after it has
#shown 3000 characters. The program should retrieve the entire document and count the total number of characters and display the count
#of the number of characters at the end of the document.
import sys
import socket
try:
	url = input('Enter an URL ')
	if(url==""):
		url="http://data.pr4e.org/romeo.txt"
	hostname=url.split('/')
	if(hostname[0]!="http:" and hostname[0]!="https:"):
		print("You have input an improperly formatted or non-existent URL. ")
		exit()
	if((hostname[0]=="http:" or hostname[0]=="https:") and hostname[1]==''):
		hostname=hostname[2]
		
	print("Hostname ",hostname)

	mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	mysock.connect((hostname, 80))
	cmd = 'GET '+str(url)+' HTTP/1.0\r\n\r\n'
	cmd = cmd.encode()
	mysock.send(cmd)
	count=0
	firsthit=True
	while True:
		data = mysock.recv(512)
		if len(data) < 1:
			break
		if(count+len(data)>3000):
			howmuchtoprint=3000-count
			datastr=data.decode()
			datastr=datastr[0:howmuchtoprint]
			if(firsthit==True):
				print(datastr,end='')
				firsthit=False
		else:
			print(data.decode(),end='')
		count+=len(data)

	mysock.close()
	print("Characters gotten: ",count)
except:
    print("Unexpected error:", sys.exc_info()[0])

#Exercise 3: Use urllib to replicate the previous exercise of (1) retrieving
#the document from a URL, (2) displaying up to 3000 characters, and
#(3) counting the overall number of characters in the document. Don’t
#worry about the headers for this exercise, simply show the first 3000
#characters of the document contents.
import sys
import urllib.request
try:
	url = input('Enter an URL ')
	if(url==""):
		url="http://data.pr4e.org/romeo.txt"
	hostname=url.split('/')
	if((hostname[0]!="http:" and hostname[0]!="https:")):
		print("You have input an improperly formatted or non-existent URL. ")
		exit()
	if((hostname[0]=="http:" or hostname[0]=="https:") and hostname[1]==''):
		hostname=hostname[2]
		
	print("Hostname ",hostname)
	count=0
	firsthit=True
	
	fhand = urllib.request.urlopen(url)
	for line in fhand:
		if(count+len(line.decode().strip())>3000):
			howmuchtoprint=3000-count
			datastr=line.decode().strip()
			datastr=datastr[0:howmuchtoprint]
			if(firsthit==True):
				print(datastr)
				firsthit=False
		else:
			print(line.decode().strip())
		count+=len(line.decode().strip())
	
	print("Characters gotten: ",count)
		
except:
    print("Unexpected error:", sys.exc_info()[0])



#Exercise 4: Change the urllinks.py program to extract and count paragraph (p) tags from the retrieved HTML document and display the
#count of the paragraphs as the output of your program. Do not display
#the paragraph text, only count them. Test your program on several
#small web pages as well as some larger web pages.
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl
# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
url = input('Enter - ')
if(url==""):
	url="http://data.pr4e.org/romeo.txt"
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')
# Retrieve all of the anchor tags
tags = soup('p')
print("Counted tags: "+str(len(tags)))
#for tag in tags:
#	print(tag.get('href', None))


#Exercise 5: (Advanced) Change the socket program so that it only shows
#data after the headers and a blank line have been received. Remember
#that recv receives characters (newlines and all), not lines.
import sys
import socket
#try:
url = input('Enter an URL ')
if(url==""):
	url="http://data.pr4e.org/romeo.txt"
hostname=url.split('/')
if(hostname[0]!="http:" and hostname[0]!="https:"):
	print("You have input an improperly formatted or non-existent URL. ")
	exit()
if((hostname[0]=="http:" or hostname[0]=="https:") and hostname[1]==''):
	hostname=hostname[2]
	

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect((hostname, 80))
cmd = 'GET '+str(url)+' HTTP/1.0\r\n\r\n'
cmd = cmd.encode()
mysock.send(cmd)

firsthit=True
while True:
	data = mysock.recv(512)
	if len(data) < 1:
		break
	datastr=str(data.decode())
	if(firsthit==True):
		num=datastr.find("text/plain")
		datastr=datastr[num+14:]
		firsthit=False
		print(datastr)
	else:
		print(data.decode(),end='')

mysock.close()
#except:
    #print("Unexpected error:", sys.exc_info()[0])