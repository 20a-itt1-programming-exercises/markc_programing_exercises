#Exercise 0 - Programming exercise - Person class
import datetime

class Person():

	def __init__(self, name, birthdate, address, gender):
		self.fullname=name
		self.birthdate=birthdate
		self.address=address
		self.gender=gender
		print(name," constructed")
		
	def get_firstname(self):
		firstname=self.fullname.split(" ")
		firstname=firstname[0]
		return str(firstname)
	
	def get_lastname(self):
		names=self.fullname.split(" ")
		lastname=names[len(names)-1]
		return str(lastname)
	
	def get_age(self):
		birth_date = datetime.date(int(str(self.birthdate)[:4]), int(str(self.birthdate)[5:6]), int(str(self.birthdate)[7:8]))
		end_date = datetime.date.today()

		time_difference = end_date - birth_date
		
		age = time_difference.days
		age=age/365
		return int(age)
		
	def get_address(self):
		address=self.address.split(" ")
		return address
	
	def get_full_info(self):
		full_info=dict()
		full_info["firstname"]=self.get_firstname()
		full_info["lastname"]=self.get_lastname()
		full_info["age"]=self.get_age()
		full_info["address"]=self.get_address()
		return full_info



#Exercise 1 - Programming exercise - Person Class extension

class Extended_Person(Person):
	
	def __init__(self, fullname, birthdate, address, gender, pet_type, pet_name, monthly_income):
		super().__init__(fullname, birthdate, address, gender)
		self.pet_type = pet_type
		self.pet_name = pet_name
		self.monthly_income = monthly_income

	def get_pet(self):
		return str(self.pet_type + self.pet_name)
		
	def get_income(self):
		if(self.monthly_income<10000):
			return "low"
		elif(self.monthly_income>=10000 and self.monthly_income<=20000):
			return "middle"
		elif(self.monthly_income>20000):
			return "high"

	def get_full_info(self):
		full_info=super().get_full_info()
		full_info["pet"]=self.get_pet
		full_info["income level"]=self.get_income
		return full_info
		
def main():
	person=Person("Mark Christiansen", 19860329, "Aninevej 4 5700 Svendborg DK", "Male")
	full_info=person.get_full_info()
	print(full_info)

	person_w_pet_1 = Extended_Person('John Doe', '19971114', 'Testvej 2 Odense 5000 DK', 'male', 'cat', 'meow', 5000)
	full_info=person_w_pet_1.get_full_info()
	print(full_info)
	
	