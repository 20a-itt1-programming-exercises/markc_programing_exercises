#Exercise 1 - Pair programming challenge
# Write a function
def is_leap(year):
    leap = False
    if(year % 4==0):
        if(year % 100==0 and year % 400!=0):
            leap = False
        else:
            leap = True
    return leap

year = int(raw_input())
print is_leap(year)

# Python If-Else
if (n % 2 == 0):
    if (n >= 2 and n <= 5):
        print('Not Weird')
    elif (n >= 6 and n <= 20):
        print('Weird')
    else:
        print('Not Weird')
else:
    print('Weird')

