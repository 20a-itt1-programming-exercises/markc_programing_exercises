#Exercise 1: Write a program to read through a file and print the contents
#of the file (line by line) all in upper case. Executing the program will
#look as follows:

fhand = open('mbox-short.txt')
for line in fhand:
	print(line.upper())

#Exercise 2: Write a program to prompt for a file name, and then read
#through the file and look for lines of the form:

fname = input("Input a filename:")
fhand = open(fname)
spamconf=0
spamconftotal=0
count=0
for line in fhand:
	if(line.startswith("X-DSPAM-Confidence:")):
		tempspamconf=float(line[19:])
		spamconftotal=spamconftotal+tempspamconf
		count=count+1
print("Average spam confidence: "+str(spamconftotal/count))
	
#Exercise 3: Sometimes when programmers get bored or want to have a
#bit of fun, they add a harmless Easter Egg to their program. Modify
#the program that prompts the user for the file name so that it prints a
#funny message when the user types in the exact file name “na na boo
#boo”. The program should behave normally for all other files which
#exist and don’t exist. Here is a sample execution of the program:

fname = input("Input a filename:")
if(fname=="na na boo boo"):
	print("NA NA BOO BOO TO YOU - You have been punk'd!")
	exit()
fhand = open(fname)
count=0
for line in fhand:
	count=count+1
print(f"There were {count} subject lines in {fname}")
