# Exercise 0 - Python for everybody chapter 1 knowledge
# What the Python interpreter is and how it differs from a compiler?
# Interpretor is one line 
# Compiler compiles multiline 

# What is the difference between syntax errors, logic errors and semantic errors?
# Syntax errors are where your code dont follow the standard of coding, when its missing a sign or like that.
# Logic errors is when the program works, but it does the wrong thing.
# Semantic errors i guess is like the syntax errors

# What is a program?
# A program is software on a computer that does what it is instructed to do in the code.

# What is input and output?
# Input is what you put in either via keyboard or sensors
# Output is what the program does or prints out

# What is sequential execution?
# Means that the program starts from line 1 and execute it, continues to line 2 and so on.

# Which 4 things should you do when debugging?
# Reading, Running, Ruminating and retreating

# Exercise 1 - Python for everybody chapter 2 knowledge - part 1
# What types of values is Python using?
# str, int, float, complex?

# How would you check a values type in a Python program?
# type(obj)

# What are variables?
# test = "" <- That is a variable

# What are reserved words?
# Reserved words are words python use, so you cant use them as a variable, fx is

# What is a statement?
# x=2 is and assignment statement
# print(1) is an expression statement

# Exercise 2 - Python for everybody chapter 2 knowledge - part 2
# What is the purpose of mnemonic naming?
# You name your variables smart so you can remember them later and easily read the code again

# Give at least 3 examples of mnemonic naming.
# patients, animals, days

# Name pythons 6 operators and their syntax
# + - * / // %

# What is order of operations?
# Like Math, */ before +-

# What is concatenation?
# when you add two strings

# How do you ask the user for input in a Python program?
# input("")

# How do you insert comments in a Python program?
# By putting # first in the line