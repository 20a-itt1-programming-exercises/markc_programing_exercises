# Exercise 2 - Pair programming challenge
# Define 4 functions to add, subtract, divide or multiply 2 float numbers
# If the user inputs done at any time the program will stop and print the message goodbye and exit
# If the user enters anything else than numbers or done the program should catch a ValueError and print the message only numbers and "done" is accepted as input, please try again and then restart
# If the user tries to divide by zero the program should catch a ZeroDivisionError and print the message cannot divide by zero, please try again and then restart
# On successful calculation the program should restart and ask the user to enter new numbers again

def add(firstnumber, secondnumber):
	return float(firstnumber)+float(secondnumber)
	
def subtract(firstnumber, secondnumber):
	return float(firstnumber)-float(secondnumber)
	
def divide (firstnumber, secondnumber):
	return float(firstnumber)/float(secondnumber)
	
def multiply (firstnumber, secondnumber):
	return float(firstnumber)*float(secondnumber)
firstnumber=""
secondnumber=""
do=""
done=False
while(done==False):
	firstnumber=input("enter the first number: ")
	if(firstnumber=="done"):
		done=True
		break
	secondnumber=input("enter the second number: ")
	if(secondnumber=="done"):
		done=True
		break
	do=input("would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers? ")	
	if(do=="done"):
		done=True
		break
	elif(int(do)==1):
		print(add(firstnumber, secondnumber))
	elif(int(do)==2):
		print(subtract(firstnumber, secondnumber))
	elif(int(do)==3):
		print(divide(firstnumber, secondnumber))
	elif(int(do)==4):
		print(multiply(firstnumber, secondnumber))
if(done==True):
	print('goodbye')	
	
	while(done==False):
		if(do=="done"):
			done=True
			break
