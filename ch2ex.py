# Exercises
# Exercise 2: Write a program that uses input to prompt a user for their
# name and then welcomes them.
xname=input('Please write your name')
print("Welcome "+xname)

# Exercise 3: Write a program to prompt the user for hours and rate per
# hour to compute gross pay.
xhours=input('Write your number of hours')
xpayprhour=input('Write your hourlyrate')
xtotalpay=xpayprhour*xhours
print('Your gross pay is '+xtotalpay)

# Exercise 4: Assume that we execute the following assignment state-
# ments:
width = 17
height = 12.0
# 1. 8 int
# 2. 8,5 float
# 3. 4.0 float
# 4. 11 int 

# Exercise 5: Write a program which prompts the user for a Celsius tem-
# perature, convert the temperature to Fahrenheit, and print out the
# converted temperature.
cels=input('Write a Celsius temperature')
fahr=cels*1.8+32
print(fahr)