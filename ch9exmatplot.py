#Make a line chart of the data from PY4E ch9_ex2 or ch9_ex5 following video1 https://youtu.be/UO98lJQ3QGI just skip the install part if you are on pycharm (start at 2:00)
#Important! matplotlib uses seperate lists for the x and y axis.
#ch9_ex2 and ch9_ex5 will give you a dictionary that you have to convert into one list containing the keys and another list containing the values.
#Make a bar chart of the data from PY4E ch9_ex2 or ch9_ex5 following video1 https://youtu.be/UO98lJQ3QGI

#Exercise 2: Write a program that categorizes each mail message by
#which day of the week the commit was done. To do this look for lines
#that start with “From”, then look for the third word and keep a running
#count of each of the days of the week. At the end of the program print
#out the contents of your dictionary (order does not matter).
#Sample Line:
#From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008
#Sample Execution:
#python dow.py
#Enter a file name: mbox-short.txt
#{'Fri': 20, 'Thu': 6, 'Sat': 1}

from matplotlib import pyplot as plt

fhand = open('mbox-short.txt')
days={'Mon':0,'Tue':0,'Wed':0,'Thu':0,'Fri':0,'Sat':0,'Sun':0}
for line in fhand:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	days[words[2]] = days[words[2]] + 1
days_list=list()
count_list=list()
for day, count in list(days.items()):
	days_list.append(day)
	count_list.append(count)


plt.plot(days_list,count_list)
#plt.barh(days_list, count_list)
plt.title('Mails pr day')
plt.show()
	

#Exercise 5: This program records the domain name (instead of the
#address) where the message was sent from instead of who the mail came
#from (i.e., the whole email address). At the end of the program, print
#out the contents of your dictionary.
from matplotlib import pyplot as plt

fhand = open('mbox-short.txt')
mails=dict()
for line in fhand:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	frommail=words[1].split('@')
	frommail=frommail[1]
	if(frommail not in mails):
		mails[frommail]=1
	else:
		mails[frommail] = mails[frommail] + 1
mail_list=list()
count_list=list()
for mail, count in list(mails.items()):
	mail_list.append(mail)
	count_list.append(count)


#plt.plot(mail_list,count_list)
plt.barh(mail_list, count_list)
plt.title('Count from mail')
plt.show()