#Exercise 1: Download a copy of the file www.py4e.com/code3/words.txt
#Write a program that reads the words in words.txt and stores them as
#keys in a dictionary. It doesn’t matter what the values are. Then you
#can use the in operator as a fast way to check whether a string is in the
#dictionary
fhand = open('words.txt')
worddict=dict()
for line in fhand:
	words = line.split()
	if len(words) == 0 : continue
	for word in words:
		worddict[word]=''
print(worddict)
			
#Exercise 2: Write a program that categorizes each mail message by
#which day of the week the commit was done. To do this look for lines
#that start with “From”, then look for the third word and keep a running
#count of each of the days of the week. At the end of the program print
#out the contents of your dictionary (order does not matter).
#Sample Line:
#From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008
#Sample Execution:
#python dow.py
#Enter a file name: mbox-short.txt
#{'Fri': 20, 'Thu': 6, 'Sat': 1}

fhand = open('mbox-short.txt')
days={'Mon':0,'Tue':0,'Wed':0,'Thu':0,'Fri':0,'Sat':0,'Sun':0}
for line in fhand:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	days[words[2]] = days[words[2]] + 1
print(days)
	
#Exercise 3: Write a program to read through a mail log, build a histogram using a dictionary to count how many messages have come from
#each email address, and print the dictionary.
#Enter file name: mbox-short.txt
#{'gopal.ramasammycook@gmail.com': 1, 'louis@media.berkeley.edu': 3,
#'cwen@iupui.edu': 5, 'antranig@caret.cam.ac.uk': 1,
#'rjlowe@iupui.edu': 2, 'gsilver@umich.edu': 3,
#'david.horwitz@uct.ac.za': 4, 'wagnermr@iupui.edu': 1,
#'zqian@umich.edu': 4, 'stephen.marquard@uct.ac.za': 2,
#'ray@media.berkeley.edu': 1}
fhand = open('mbox-short.txt')
mails=dict()
for line in fhand:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	if(words[1] not in mails):
		mails[words[1]]=1
	else:
		mails[words[1]] = mails[words[1]] + 1
print(mails)

#Exercise 4: Add code to the above program to figure out who has the
#most messages in the file. After all the data has been read and the dictionary has been created, look through the dictionary using a maximum
#loop (see Chapter 5: Maximum and minimum loops) to find who has
#the most messages and print how many messages the person has.
import operator
fhand = open('mbox-short.txt')
mails=dict()
for line in fhand:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	if(words[1] not in mails):
		mails[words[1]]=1
	else:
		mails[words[1]] = mails[words[1]] + 1
maxmails=max(mails.items(), key=operator.itemgetter(1))[0]
print(maxmails," ",mails[maxmails])

#Exercise 5: This program records the domain name (instead of the
#address) where the message was sent from instead of who the mail came
#from (i.e., the whole email address). At the end of the program, print
#out the contents of your dictionary.

fhand = open('mbox-short.txt')
mails=dict()
for line in fhand:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	frommail=words[1].split('@')
	frommail=frommail[1]
	if(frommail not in mails):
		mails[frommail]=1
	else:
		mails[frommail] = mails[frommail] + 1
print(mails)