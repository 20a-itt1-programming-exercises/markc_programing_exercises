# 3.11 Exercises
# Exercise 1: Rewrite your pay computation to give the employee 1.5
# times the hourly rate for hours worked above 40 hours.

hours=input('Write your number of hours')
rate=input('Write your hourlyrate')
pay=0
overtimerate=1.5
if(hours<40):
	pay=hours*rate
else:
	pay=40*rate+((hours-40)*rate*overtimerate)
print('Total pay: '+round(pay, 2))

# Exercise 2: Rewrite your pay program using try and except so that your
# program handles non-numeric input gracefully by printing a message
# and exiting the program. The following shows two executions of the program:

try:
	hours=int(input('Write your number of hours'))
	rate=float(input('Write your hourlyrate'))
	pay=0
	overtimerate=1.5
	if(hours<40):
		pay=hours*rate
	else:
		pay=40*rate+((hours-40)*rate*overtimerate)
	print('Total pay: '+round(pay, 2))
except:
	print('Please enter a correct number')

# Exercise 3: Write a program to prompt for a score between 0.0 and
# 1.0. If the score is out of range, print an error message. If the score is
# between 0.0 and 1.0, print a grade using the following table:
try:
	score=float(input('Please write a score between 0.0 and 1.0'))
	if(score>1.0 || score<0.0):
		print('Please place a correct score')
	elif(score>=0.9):
		print('A')
	elif(score>=0.8):
		print('B')
	elif(score>=0.7):
		print('C')
	elif(score>=0.6):
		print('D')
	elif(score<0.6):
		print('F')
except:
	print('Please place a correct score')