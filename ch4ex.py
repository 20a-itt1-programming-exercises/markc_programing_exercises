# Exercises
# Exercise 4: What is the purpose of the “def” keyword in Python?
# a) It is slang that means “the following code is really cool”
# b) It indicates the start of a function
# c) It indicates that the following indented section of code is to be stored for later
# d) b and c are both true
# e) None of the above
# Answer is b.

# Exercise 5: What will the following Python program print out?
def fred():
	print("Zap")
def jane():
	print("ABC")
jane()
fred()
jane()

# a) Zap ABC jane fred jane
# b) Zap ABC Zap
# c) ABC Zap jane
# d) ABC Zap ABC
# e) Zap Zap Zap

# Answer is d) ABC Zap ABC


# Exercise 6: Rewrite your pay computation with time-and-a-half for overtime and create a function called computepay which takes two parameters
# (hours and rate).
# Enter Hours: 45
# Enter Rate: 10
# Pay: 475.0

def computepay(hours, rate):
	pay=0
	overtimerate=1.5
	weeklyhours=40
	if(hours<weeklyhours):
		pay=hours*rate
	else:
		pay=weeklyhours*rate+((hours-weeklyhours)*rate*overtimerate)
	return pay

try:
	hours=int(input('Write your number of hours'))
	rate=float(input('Write your hourlyrate'))
	pay = computepay(hours, rate)
	print('Total pay: '+round(pay, 2))
except:
	print('Please enter a correct number')

# Exercise 7: Rewrite the grade program from the previous chapter using
# a function called computegrade that takes a score as its parameter and
# returns a grade as a string

def computegrade(score):
	try:
		score = float(score)
		grade="";
		if(score>1.0 or score<0.0):
			grade='Please place a correct score'
		elif(score>=0.9):
			grade='A'
		elif(score>=0.8):
			grade='B'
		elif(score>=0.7):
			grade='C'
		elif(score>=0.6):
			grade='D'
		elif(score<0.6):
			grade='F'
		return grade
	except:
		print('Please place a correct score')
		return false

score=input('Please write a score between 0.0 and 1.0')
grade=computegrade(score)
print('Your grade is '+grade)